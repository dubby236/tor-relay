# tor-relay  

This ansible-play will install bootstrap a centos 7 machine and install a tor-relay.  

## Included tasks  

- set hostname (vars)  
- update && upgrade OS  
- auto security updates  
- install EPEL7 Repo  
- install packages (vars)  
- create new user for ssh access (vars)  
- sshd config (vars)  
- enable firewall (vars)  
- install and config fail2ban for ssh  

## Usage  
- adjust files: hosts + vars.yml  
- to bootstrap a fresh install run:  
  `ansible-playbook -i hosts playbooks/tor-relay.yml -e "@vars.yml" -k -K -D --user root`
- after bootstrapping:  
  `ansible-playbook -i hosts playbooks/tor-relay.yml -e "@vars.yml" -D --user user --ssh-extra-args "-p ssh.port"`
